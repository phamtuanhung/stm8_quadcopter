/* stm8_sensor.c */

#include "stm8_sensor.h"

static sensor_data_t sensor_data_offset;

static void delay (int time_ms) {
  volatile long int x;
  for (x = 0; x < 200*time_ms; ++x)
    x=x;
}

void stm8_sensor_init(void)
{
  stm8_i2c_init();
  stm8_sensor_mpu6050_init();
  stm8_sensor_mpu6050_calibrate();
}

void stm8_sensor_mpu6050_init(void)
{
  stm8_i2c_set_stop();
  delay(100);
  stm8_i2c_set_start_ack();
  stm8_i2c_send_address (STM8_I2C_WRITE, MPU6050_ADDR);
  stm8_i2c_send_reg(0x6B);
  stm8_i2c_send_reg(0x80);
  stm8_i2c_set_stop();
  delay(100);
  
  stm8_i2c_set_start_ack();
  stm8_i2c_send_address (STM8_I2C_WRITE, MPU6050_ADDR);
  stm8_i2c_send_reg(0x6B);
  stm8_i2c_send_reg(0x00);
  stm8_i2c_set_stop();
  delay(100);
  
  // set accel scale
  stm8_i2c_set_start_ack();
  stm8_i2c_send_address (STM8_I2C_WRITE, MPU6050_ADDR);
  stm8_i2c_send_reg(0x1C);
  stm8_i2c_send_reg(0x18);
  stm8_i2c_set_stop();
  delay(100);
}

void stm8_sensor_mpu6050_calibrate(void)
{
    stm8_i2c_set_start_ack();
    stm8_i2c_send_address (STM8_I2C_WRITE, MPU6050_ADDR);
    stm8_i2c_send_reg(0x1A);
    stm8_i2c_send_reg(0x04);
    stm8_i2c_set_stop();
    delay(100);
    
    sensor_data_offset.accel_x = 0;
    sensor_data_offset.accel_y = 0;
    sensor_data_offset.accel_z = 0;
    
    sensor_data_offset.gyro_x  = 0;
    sensor_data_offset.gyro_y  = 0;
    sensor_data_offset.gyro_z  = 0;
    
    sensor_data_t sensor_data;
    for (int i = 0; i < 10; i++)
    {
        sensor_data = stm8_sensor_get_value();
        sensor_data_offset.accel_x = sensor_data_offset.accel_x + sensor_data.accel_x;
        sensor_data_offset.accel_y = sensor_data_offset.accel_y + sensor_data.accel_y;
        sensor_data_offset.accel_z = sensor_data_offset.accel_z + sensor_data.accel_z - 2048;
        
        sensor_data_offset.gyro_x = sensor_data_offset.gyro_x+sensor_data.gyro_x;
        sensor_data_offset.gyro_y = sensor_data_offset.gyro_y+sensor_data.gyro_y;
        sensor_data_offset.gyro_z = sensor_data_offset.gyro_z+sensor_data.gyro_z;
        delay(100);
    }
    sensor_data_offset.accel_x = sensor_data_offset.accel_x / 10;
    sensor_data_offset.accel_y = sensor_data_offset.accel_y / 10;
    sensor_data_offset.accel_z = sensor_data_offset.accel_z / 10;
    
    sensor_data_offset.gyro_x  = sensor_data_offset.gyro_x / 10;
    sensor_data_offset.gyro_y  = sensor_data_offset.gyro_y / 10;
    sensor_data_offset.gyro_z  = sensor_data_offset.gyro_z / 10;
    
    sensor_data_offset = stm8_sensor_get_value();
    sensor_data_offset.accel_z = sensor_data_offset.accel_z - 2048;
}

sensor_data_t stm8_sensor_get_value(void){
  sensor_data_t sensor_data;
  uint8_t xh,xl; 
  
  xh = stm8_i2c_read_register (ACCEL_XOUT_H, MPU6050_ADDR);
  xl = stm8_i2c_read_register (ACCEL_XOUT_L, MPU6050_ADDR);
  sensor_data.accel_x = xh << 8 | xl;
  xh = stm8_i2c_read_register (ACCEL_YOUT_H, MPU6050_ADDR);
  xl = stm8_i2c_read_register (ACCEL_YOUT_L, MPU6050_ADDR);
  sensor_data.accel_y = xh << 8 | xl;
  xh = stm8_i2c_read_register (ACCEL_ZOUT_H, MPU6050_ADDR);
  xl = stm8_i2c_read_register (ACCEL_ZOUT_L, MPU6050_ADDR);
  sensor_data.accel_z = xh << 8 | xl;
  
  xh = stm8_i2c_read_register (GYRO_XOUT_H, MPU6050_ADDR);
  xl = stm8_i2c_read_register (GYRO_XOUT_L, MPU6050_ADDR);
  sensor_data.gyro_x  = xh << 8 | xl;
  xh = stm8_i2c_read_register (GYRO_YOUT_H, MPU6050_ADDR);
  xl = stm8_i2c_read_register (GYRO_YOUT_L, MPU6050_ADDR);
  sensor_data.gyro_y  = xh << 8 | xl;
  xh = stm8_i2c_read_register (GYRO_ZOUT_H, MPU6050_ADDR);
  xl = stm8_i2c_read_register (GYRO_ZOUT_L, MPU6050_ADDR);
  sensor_data.gyro_z  = xh << 8 | xl;
  
  sensor_data.accel_x -= sensor_data_offset.accel_x;
  sensor_data.accel_y -= sensor_data_offset.accel_y;
  sensor_data.accel_z -= sensor_data_offset.accel_z;
  
  sensor_data.gyro_x -= sensor_data_offset.gyro_x;
  sensor_data.gyro_y -= sensor_data_offset.gyro_y;
  sensor_data.gyro_z -= sensor_data_offset.gyro_z;
  
  return(sensor_data);
}