
/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include <iostm8s103f3.h>
#include <intrinsics.h>

#include "stm8_io.h"
#include "stm8_timer.h"
#include "stm8_uart.h"
#include "stm8_sensor.h"
#include "stm8_ir.h"
#include "stm8_pid.h"


void interval_function(void);
void ir_exti_calback(void);
void print_float(double i);

#define TIME_OUT 1600
extern uint8_t ir_receiving;

uint16_t time_out;
uint16_t pwm = 0;

int main( void )
{
  InitialiseSystemClock();
  io_init();
  set_led(ON);
  console_init();
  uart_printf("\n\n*********************************\n");
  uart_printf("*          Quadcopter           *\n");
  uart_printf("*********************************\n");
  timer_init();
  timer_set_interval(interval_function, 20 /*ms*/);
  io_set_exti_callback(ir_exti_calback);
  i2c_sensor_init();
  __enable_interrupt();
  set_led(OFF);
  while(1)
  {
    if (pwm > 500)
      set_led(ON);
    if (ir_receiving ==1)
    {
      time_out++;
    }
    if (time_out == TIME_OUT)
    {
      time_out = 0;
      timer_interval_enable();
    }
    //uint16_t dt = timer_get_loop_time();
    //uart_printf("   loop time: ");
    //uart_print_int(dt);
    //uart_printf("\n");
    //
    //pid_update_pwm();
  }
}
void interval_function(void)
{
  //uint16_t dt = timer_get_loop_time();
  //uart_printf("   loop time: ");
  //uart_print_int(dt);
  //uart_printf("\n");
  
  pid_update_pwm();
}