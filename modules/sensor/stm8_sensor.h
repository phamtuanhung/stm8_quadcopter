/* stm8_sensor.h */

#include <stdint.h>
#include <iostm8s103f3.h>
#include "stm8_i2c.h"


#define MPU6050_ADDR     0x68

#define ACCEL_XOUT_H     0x3B
#define ACCEL_XOUT_L     0x3C
#define ACCEL_YOUT_H     0x3D
#define ACCEL_YOUT_L     0x3E
#define ACCEL_ZOUT_H     0x3F
#define ACCEL_ZOUT_L     0x40
#define TEMP_OUT_H       0x41
#define TEMP_OUT_L       0x42
#define GYRO_XOUT_H      0x43
#define GYRO_XOUT_L      0x44
#define GYRO_YOUT_H      0x45
#define GYRO_YOUT_L      0x46
#define GYRO_ZOUT_H      0x47
#define GYRO_ZOUT_L      0x48

typedef struct
{
  int16_t gyro_x;
  int16_t gyro_y;
  int16_t gyro_z;
  int16_t accel_x;
  int16_t accel_y;
  int16_t accel_z;
}sensor_data_t;

void stm8_sensor_init(void);
void stm8_sensor_mpu6050_init(void);
void stm8_sensor_mpu6050_calibrate(void);
sensor_data_t stm8_sensor_get_value(void);
