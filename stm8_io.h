
#include <stdint.h>
#include <iostm8s103f3.h>

//#define LED_GPIO_PORT  (GPIOB)
#define LED_GPIO_PINS  0x04 //(GPIO_PIN_5)
//#define BUTTON_GPIO_PORT  (GPIOA)
#define BUTTON_GPIO_PINS  0x20 //( GPIO_PIN_5)
#define ON 1
#define OFF 0

typedef void (*exti_callback_function_t)(void);

void io_init(void);
void set_led(uint8_t state);
void toggle_led(void);
uint8_t io_read_data();
void io_set_exti_callback(exti_callback_function_t function);