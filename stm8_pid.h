
#include <stdint.h>
#include <iostm8s103f3.h>

#define ADJ_P 0
#define ADJ_I 1
#define ADJ_D 2

extern uint16_t pwm;

extern uint8_t k_adjust;
extern double adjust;

void pid_update_pwm(void);
void pid_calibrate(void);
void pid_adjust_k(int8_t c);
void pid_print_k(void);