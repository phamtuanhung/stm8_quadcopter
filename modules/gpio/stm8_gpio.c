/* stm8_gpio.c */

#include "stm8_gpio.h"

void stm8_gpio_init()
{
    stm8_gpio_led_init();
}

void stm8_gpio_led_init(void)
{
    PD_ODR = 0; //Turn off all pins
    PD_DDR_DDR2 = 1; //PortD, Bit 2 is output
}

void stm8_gpio_set_led(uint8_t state)
{
  if (state == ON)
    PD_ODR &= (uint8_t)(~STM8_GPIO_LED_PIN);
  else
    PD_ODR |= (uint8_t)STM8_GPIO_LED_PIN;
}
void stm8_gpio_toggle_led(void)
{
  PD_ODR ^= (uint8_t)STM8_GPIO_LED_PIN;
}