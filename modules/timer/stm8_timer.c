/* stm8_timer.c */

#include "stm8_timer.h"

stm8_timer_interval_callback_function_t pIntervalCallbackPointer;
uint16_t gIntervalValue = 10;
uint16_t gIntervalCounter = 0;
uint16_t gLastCounterValue;

void stm8_timer_delay(uint16_t ms)
{
    /* Decrement ms value */
    while (ms != 0)
    {
        ms--;
        uint16_t count = 1000;
        while (count != 0)
        {
            count--;
        }
    }
}
void stm8_timer_init(void)
{
    // Init interval timer
    stm8_timer_interval_init();
    pIntervalCallbackPointer = stm8_timer_interval_default_function;
    
    // Init counter timer
    stm8_timer_counter_init();
    gLastCounterValue = stm8_timer_counter_get_value();
}

void stm8_timer_interval_init(void)
{
    /* Set the Autoreload value */
    TIM1_ARRH = (uint8_t)(1000 >> 8);
    TIM1_ARRL = (uint8_t)(1000);  
    /* Set the Prescaler value */
    TIM1_PSCRH = (uint8_t)(0 >> 8);
    TIM1_PSCRL = (uint8_t)(13);  
    /* Set the Repetition Counter value */
    TIM1_RCR = 0; 
    
    TIM1_IER |= (uint8_t)0x01;
    
    /*TIM1 counter enable */
    TIM1_CR1 |= 0x01;
}

void stm8_timer_interval_enable()
{
  TIM1_IER |= (uint8_t)0x01;
}
void stm8_timer_interval_disable()
{
  TIM1_IER &= (uint8_t)(~0x01);
}

void stm8_timer_set_interval(stm8_timer_interval_callback_function_t function, uint16_t ms)
{
    pIntervalCallbackPointer = function;
    gIntervalValue = ms;
}
void stm8_timer_interval_default_function(void)
{
  
}

#pragma vector = 13
__interrupt void TIM1_UPD_OVF_TRG_BRK_IRQHandler(void)
{
    TIM1_SR1 = (uint8_t)(~0x01);
    gIntervalCounter++;
    if ((gIntervalCounter%gIntervalValue)==0)
    {
    (*pIntervalCallbackPointer)();
    }
    if (gIntervalCounter == 500)
    {
    gIntervalCounter = 0;
    }
}

void stm8_timer_counter_init(void)
{
    /* TIM2 Peripheral Configuration */ 
    /* Set the Prescaler value */
    TIM2_PSCR = (uint8_t)0x07;//(TIM2_PRESCALER_128);
    TIM2_ARRH = 0xFF;
    TIM2_ARRL = 0xFF;
    /*TIM2 counter enable */
    TIM2_CR1 |= (uint8_t)0x01;
}

uint16_t stm8_timer_couter_get_loop(void)
{
    uint16_t currentValue = stm8_timer_counter_get_value();
    uint16_t diferenceValue = currentValue - gLastCounterValue;
    gLastCounterValue=currentValue;
    return diferenceValue;
}

uint16_t stm8_timer_counter_get_value(void)
{
    uint16_t tmpcntr = 0;  
    tmpcntr =  ((uint16_t)TIM2_CNTRH << 8);
    /* Get the Counter Register value */
    return (uint16_t)( tmpcntr| (uint16_t)(TIM2_CNTRL));
}
