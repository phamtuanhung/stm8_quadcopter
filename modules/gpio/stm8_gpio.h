/* stm8_gpio.h */

#include <stdint.h>
#include <iostm8s103f3.h>

#define STM8_GPIO_LED_PIN  0x04 //(D2)
#define ON 1
#define OFF 0

void stm8_gpio_init();
void stm8_gpio_led_init(void);
void stm8_gpio_set_led(uint8_t state);
void stm8_gpio_toggle_led(void);
