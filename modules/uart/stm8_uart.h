/* stm8_uart.h */

#include <stdio.h>
#include <iostm8s103f3.h>
#include <stdlib.h>

void stm8_uart_init(void);
void stm8_uart_printf(char *message);
void stm8_uart_put_char(unsigned char c);
void stm8_uart_print_float(double f);
void stm8_uart_print_int(int i);
void InitialiseSystemClock(void);
