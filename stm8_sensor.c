
#include "stm8_sensor.h"
#include "stm8_uart.h"

#define SET(x, y)   (x) |= (y)
#define UNSET(x, y) (x) &= ~(y)
#define READ(x, y)  ((x) & (y))
#define I2C_READ        1
#define I2C_WRITE       0
#define MPU6050_ADDR     0x68 
#define MPU_TIME_OUT 1600
#define ERROR -1;

static uint16_t time_out = 0;
static sensor_data_t sensor_data_offset;

void delay (int time_ms) {
  volatile long int x;
  for (x = 0; x < 200*time_ms; ++x)
    x=x;
}

int8_t i2c_read (unsigned char *x) {
  time_out = 0;
  while((I2C_SR1 & I2C_RXNE)==0)
  {
    time_out++;
    if (time_out == MPU_TIME_OUT)
      return ERROR;
  }
  *x = I2C_DR;
  return 0;
}
void i2c_set_nak (void) {
  I2C_CR2 &= ~I2C_ACK;
}
void i2c_set_stop (void) {
  I2C_CR2 |= I2C_STOP;
}
int8_t i2c_send_reg (uint8_t rg) {
  volatile int reg;
  reg = I2C_SR1;
  reg = I2C_SR3;
  I2C_DR = rg;
  time_out = 0;
  while((I2C_SR1 & I2C_TXE)==0)
  {
    time_out++;
    if (time_out == MPU_TIME_OUT)
      return ERROR;
  }
  return 0;
}

int8_t i2c_send_address (uint8_t mode) {
  volatile int reg;
  reg = I2C_SR1;
  I2C_DR = (MPU6050_ADDR << 1) | mode;
  if (mode == I2C_READ) {
    I2C_OARL = 0;
    I2C_OARH = 0;
  }
  
  time_out = 0;
  while((I2C_SR1 & I2C_ADDR)==0)
  {
    time_out++;
    if (time_out == MPU_TIME_OUT)
      return ERROR;
  }
  if (mode == I2C_READ)
    UNSET(I2C_SR1, I2C_ADDR);
  return 0;
}

int8_t i2c_set_start_ack (void) {
  I2C_CR2 = I2C_ACK | I2C_START;
  time_out = 0;
  while((I2C_SR1 & I2C_SB)==0)
  {
    time_out++;
    if (time_out == MPU_TIME_OUT)
      return ERROR;
  }
  return 0;
}

void i2c_wait(uint8_t wait)
{
  //uint16_t time_out;
  while (wait == 0)
  {
  //  time_out++;
  //  if (time_out == 1600)
  //    break;
  }
}

unsigned char i2c_read_register (uint8_t rg) {
  volatile uint8_t reg;
  uint8_t x;
  int8_t error = 0;
  error |= i2c_set_start_ack ();
  error |= i2c_send_address (I2C_WRITE);
  error |= i2c_send_reg (rg);
  error |= i2c_set_start_ack ();
  error |= i2c_send_address (I2C_READ);
  
  if (error!=0)
    i2c_mpu6050_init();
  
  reg = I2C_SR1;
  reg = I2C_SR3;
  i2c_set_nak ();
  i2c_set_stop ();
  i2c_read (&x);
  return (x);
}

void i2c_sensor_init(void)
{
  uart_printf("Sensor init\n");
  i2c_init();
  i2c_mpu6050_init();
  i2c_mpu6050_calibrate();
}
void i2c_init (void) {
  I2C_CR1 = 0;   //  Disable I2C before configuration starts. PE bit is bit 0
  I2C_FREQR = 16;                     //  Set the internal clock frequency (MHz).
  UNSET (I2C_CCRH, I2C_FS);           //  I2C running is standard mode.
  I2C_CCRL = 0x10;                    //  SCL clock speed is 500 kHz.
  I2C_CCRH &= 0x00;	// Clears lower 4 bits "CCR"
  UNSET (I2C_OARH, I2C_ADDMODE);      //  7 bit address mode.
  SET (I2C_OARH, I2C_ADDCONF);        //  Docs say this must always be 1.
  I2C_TRISER = 17;
  I2C_CR1 = I2C_PE;	// Enables port
}

void i2c_mpu6050_init(void)
{
  uart_printf("MPU init\n");
  i2c_set_stop();
  delay(100);
  i2c_set_start_ack();
  i2c_send_address (I2C_WRITE);
  i2c_send_reg(0x6B);
  i2c_send_reg(0x80);
  i2c_set_stop();
  delay(100);
  
  i2c_set_start_ack();
  i2c_send_address (I2C_WRITE);
  i2c_send_reg(0x6B);
  i2c_send_reg(0x00);
  i2c_set_stop();
  delay(100);
  
  uart_printf("MPU init finished\n");
  
  unsigned char accel_scale = i2c_read_register (0x1C);
  uart_printf("accel_scale before: ");
  uart_print_int(accel_scale);
  uart_printf("\n");
  delay(100);
  
  i2c_set_start_ack();
  i2c_send_address(I2C_WRITE);
  i2c_send_reg(0x1C);
  i2c_send_reg(0x18);
  i2c_set_stop();
  delay(100);
  
  accel_scale = i2c_read_register (0x1C);
  uart_printf("accel_scale after: ");
  uart_print_int(accel_scale);
  uart_printf("\n");
  delay(100);
  
}

void i2c_mpu6050_calibrate(void)
{
    uart_printf("Sensor init ");
    sensor_data_offset.accel_x = 0;
    sensor_data_offset.accel_y = 0;
    sensor_data_offset.accel_z = 0;
    
    sensor_data_offset.gyro_x  = 0;
    sensor_data_offset.gyro_y  = 0;
    sensor_data_offset.gyro_z  = 0;
    
    sensor_data_t sensor_data;
    for (int i = 0; i < 10; i++)
    {
        sensor_data = i2c_get_mpu6050();
        sensor_data_offset.accel_x = sensor_data_offset.accel_x + sensor_data.accel_x;
        sensor_data_offset.accel_y = sensor_data_offset.accel_y + sensor_data.accel_y;
        sensor_data_offset.accel_z = sensor_data_offset.accel_z + sensor_data.accel_z - 2048;
        
        sensor_data_offset.gyro_x = sensor_data_offset.gyro_x+sensor_data.gyro_x;
        sensor_data_offset.gyro_y = sensor_data_offset.gyro_y+sensor_data.gyro_y;
        sensor_data_offset.gyro_z = sensor_data_offset.gyro_z+sensor_data.gyro_z;
        delay(100);
        uart_printf(".");
    }
    sensor_data_offset.accel_x = sensor_data_offset.accel_x / 10;
    sensor_data_offset.accel_y = sensor_data_offset.accel_y / 10;
    sensor_data_offset.accel_z = sensor_data_offset.accel_z / 10;
    
    sensor_data_offset.gyro_x  = sensor_data_offset.gyro_x / 10;
    sensor_data_offset.gyro_y  = sensor_data_offset.gyro_y / 10;
    sensor_data_offset.gyro_z  = sensor_data_offset.gyro_z / 10;
    
    sensor_data_offset = i2c_get_mpu6050();
    sensor_data_offset.accel_z = sensor_data_offset.accel_z - 2048;
    uart_printf(" Done!\n");
    
}

sensor_data_t i2c_get_mpu6050(void){
  sensor_data_t sensor_data;
  uint8_t xh,xl; 
  
  xh = i2c_read_register (ACCEL_XOUT_H);
  xl = i2c_read_register (ACCEL_XOUT_L);  
  sensor_data.accel_x = xh << 8 | xl;  
  xh = i2c_read_register (ACCEL_YOUT_H);
  xl = i2c_read_register (ACCEL_YOUT_L);
  sensor_data.accel_y = xh << 8 | xl;  
  xh = i2c_read_register (ACCEL_ZOUT_H);
  xl = i2c_read_register (ACCEL_ZOUT_L);
  sensor_data.accel_z = xh << 8 | xl;
  
  xh = i2c_read_register (GYRO_XOUT_H);
  xl = i2c_read_register (GYRO_XOUT_L);
  sensor_data.gyro_x  = xh << 8 | xl;
  xh = i2c_read_register (GYRO_YOUT_H);
  xl = i2c_read_register (GYRO_YOUT_L);
  sensor_data.gyro_y  = xh << 8 | xl;
  xh = i2c_read_register (GYRO_ZOUT_H);
  xl = i2c_read_register (GYRO_ZOUT_L);
  sensor_data.gyro_z  = xh << 8 | xl;
  
  sensor_data.accel_x -= sensor_data_offset.accel_x;
  sensor_data.accel_y -= sensor_data_offset.accel_y;
  sensor_data.accel_z -= sensor_data_offset.accel_z;
  
  sensor_data.gyro_x -= sensor_data_offset.gyro_x;
  sensor_data.gyro_y -= sensor_data_offset.gyro_y;
  sensor_data.gyro_z -= sensor_data_offset.gyro_z;
  
  return(sensor_data);
}