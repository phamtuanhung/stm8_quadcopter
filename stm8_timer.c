
#include "stm8_timer.h"
#include "stm8_io.h"

interval_callback_function_t interval_callback_pointer;
uint16_t interval_ms = 10;
uint16_t last_time;
uint16_t counter_ms = 0;

void InitialiseSystemClock()
{
    CLK_ICKR = 0;                       //  Reset the Internal Clock Register.
    CLK_ICKR_HSIEN = 1;                 //  Enable the HSI.
    CLK_ECKR = 0;                       //  Disable the external clock.
    while (CLK_ICKR_HSIRDY == 0);       //  Wait for the HSI to be ready for use.
    CLK_CKDIVR = 0;                     //  Ensure the clocks are running at full speed.
    CLK_PCKENR1 = 0xff;                 //  Enable all peripheral clocks.
    CLK_PCKENR2 = 0xff;                 //  Ditto.
    CLK_CCOR = 0;                       //  Turn off CCO.
    CLK_HSITRIMR = 0;                   //  Turn off any HSIU trimming.
    CLK_SWIMCCR = 0;                    //  Set SWIM to run at clock / 2.
    CLK_SWR = 0xe1;                     //  Use HSI as the clock source.
    CLK_SWCR = 0;                       //  Reset the clock switch control register.
    CLK_SWCR_SWEN = 1;                  //  Enable switching.
    while (CLK_SWCR_SWBSY != 0);        //  Pause while the clock switch is busy.
}

/**
  * @brief Delay
  * @param nCount
  * @retval None
  */
void Delay(uint16_t nCount)
{
  /* Decrement nCount value */
  while (nCount != 0)
  {
    nCount--;
  }
}
void timer_init(void)
{
  timer_timer1_init();
  timer_pwm_init();
  
  timer_timer2_init();  
  interval_callback_pointer = timer_dump_interval;  
  last_time = timer_get_counter();
}

uint16_t timer_get_loop_time(void)
{
  uint16_t current_time = timer_get_counter();
  uint16_t diference_time = current_time - last_time;
  last_time=current_time;
  return diference_time;
}

void timer_timer1_init(void)
{
  /* Time Base configuration */
  /*
  TIM1_Period = 1000
  TIM1_Prescaler = 0
  TIM1_CounterMode = TIM1_COUNTERMODE_UP
  TIM1_RepetitionCounter = 0
  */  
  /* Set the Autoreload value */
  TIM1_ARRH = (uint8_t)(1000 >> 8);
  TIM1_ARRL = (uint8_t)(1000);  
  /* Set the Prescaler value */
  TIM1_PSCRH = (uint8_t)(0 >> 8);
  TIM1_PSCRL = (uint8_t)(13);  
  /* Set the Repetition Counter value */
  TIM1_RCR = 0; 
  
  TIM1_IER |= (uint8_t)0x01;
  
  /*TIM1 counter enable */
  TIM1_CR1 |= 0x01;
}

void timer_interval_enable()
{
  TIM1_IER |= (uint8_t)0x01;
}
void timer_interval_disable()
{
  TIM1_IER &= (uint8_t)(~0x01);
}

void timer_timer2_init(void)
{
  /* TIM2 Peripheral Configuration */ 
  /* Set the Prescaler value */
  TIM2_PSCR = (uint8_t)0x07;//(TIM2_PRESCALER_128);
  /* Set the Autoreload value */
  //TIM2->ARRH = (uint8_t)(999 >> 8);
  //TIM2->ARRL = (uint8_t)(999 &0x00FF);
  TIM2_ARRH = 0xFF;
  TIM2_ARRL = 0xFF;
  /*TIM2 counter enable */
  TIM2_CR1 |= (uint8_t)0x01;
  
  //TIM2_ITConfig(TIM2_IT_UPDATE, ENABLE);
  //TIM2_IER |= (uint8_t)0x01;
}
void timer_pwm_init()
{
  /* Disable the Channel 1: Reset the CCE Bit, Set the Output State , 
  the Output N State, the Output Polarity & the Output N Polarity*/
  TIM1_CCER1 = 0;
  TIM1_CCER2 = 0;
  /* Set the Output State & Set the Output N State & Set the Output Polarity &
  Set the Output N Polarity */
  TIM1_CCER1 |= (uint8_t)0x77;
  TIM1_CCER2 |= (uint8_t)0x77;
  /* Reset the Output Compare Bits & Set the Output Compare Mode */
  TIM1_CCMR1 |= 0x70;//((uint8_t)TIM1_OCMODE_PWM2);
  TIM1_CCMR2 |= 0x70;//((uint8_t)TIM1_OCMODE_PWM2);
  TIM1_CCMR3 |= 0x70;//((uint8_t)TIM1_OCMODE_PWM2);
  TIM1_CCMR4 |= 0x70;//((uint8_t)TIM1_OCMODE_PWM2);
  /* Reset the Output Idle state & the Output N Idle state bits */
  TIM1_OISR &= 0;
  /* Set the Output Idle state & the Output N Idle state configuration */
  TIM1_OISR |= (uint8_t)0x55;  
  /* Set the Pulse value */
  TIM1_CCR1H = 0;
  TIM1_CCR1L = 0;
  TIM1_CCR2H = 0;
  TIM1_CCR2L = 0;  
  
  /* TIM1 Main Output Enable */
  TIM1_BKR |= 0x80;
}

void timer_set_pwm1(uint16_t value)
{
/* Set the Capture Compare1 Register value */
  TIM1_CCR1H = (uint8_t)(value >> 8);
  TIM1_CCR1L = (uint8_t)(value);
}
void timer_set_pwm2(uint16_t value)
{
/* Set the Capture Compare1 Register value */
  TIM1_CCR2H = (uint8_t)(value >> 8);
  TIM1_CCR2L = (uint8_t)(value);
}
void timer_set_pwm3(uint16_t value)
{
/* Set the Capture Compare1 Register value */
  TIM1_CCR3H = (uint8_t)(value >> 8);
  TIM1_CCR3L = (uint8_t)(value);
}
void timer_set_pwm4(uint16_t value)
{
/* Set the Capture Compare1 Register value */
  TIM1_CCR4H = (uint8_t)(value >> 8);
  TIM1_CCR4L = (uint8_t)(value);
}
void timer_set_interval(interval_callback_function_t function, uint16_t ms)
{
  interval_callback_pointer = function;
  interval_ms = ms;
}
void timer_dump_interval(void)
{
  
}

//#pragma vector = 15
//__interrupt void TIM2_UPD_OVF_BRK_IRQHandler(void)
//{
//  TIM2_SR1 = (uint8_t)(~0x01);
//  (*interval_callback_pointer)();
//}

#pragma vector = 13
__interrupt void TIM1_UPD_OVF_TRG_BRK_IRQHandler(void)
{
  TIM1_SR1 = (uint8_t)(~0x01);
  counter_ms++;
  if ((counter_ms%interval_ms)==0)
  {
    (*interval_callback_pointer)();
  }
  if (counter_ms == 500)
  {
    counter_ms = 0;
    toggle_led();
  }
}

uint16_t timer_get_counter(void)
{
  uint16_t tmpcntr = 0;
  
  tmpcntr =  ((uint16_t)TIM2_CNTRH << 8);
  /* Get the Counter Register value */
  return (uint16_t)( tmpcntr| (uint16_t)(TIM2_CNTRL));
  //return counter_ms;
}