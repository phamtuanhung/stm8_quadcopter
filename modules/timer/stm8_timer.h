/* stm8_timer.h */

#include <stdint.h>
#include <iostm8s103f3.h>

typedef void (*stm8_timer_interval_callback_function_t)(void);

void stm8_timer_delay(uint16_t nCount);
void stm8_timer_init(void);
void stm8_timer_interval_init(void);
void stm8_timer_interval_enable();
void stm8_timer_interval_disable();
void stm8_timer_set_interval(stm8_timer_interval_callback_function_t function, uint16_t ms);
void stm8_timer_interval_default_function(void);
void stm8_timer_counter_init(void);
uint16_t stm8_timer_couter_get_loop(void);
uint16_t stm8_timer_counter_get_value(void);

