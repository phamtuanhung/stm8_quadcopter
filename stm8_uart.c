/**
  ******************************************************************************
  * @file UART1_Printf\main.c
  * @brief This file contains the main function for: retarget the C library printf
  *        /scanf functions to the UART1 example.
  * @author  MCD Application Team
  * @version V2.0.4
  * @date     26-April-2018
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2014 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/

#include <stdlib.h>
#include "stm8_uart.h"

#ifdef _RAISONANCE_
#define PUTCHAR_PROTOTYPE int putchar (char c)
#define GETCHAR_PROTOTYPE int getchar (void)
#elif defined (_COSMIC_)
#define PUTCHAR_PROTOTYPE char putchar (char c)
#define GETCHAR_PROTOTYPE char getchar (void)
#else /* _IAR_ */
#define PUTCHAR_PROTOTYPE int putchar (int c)
#define GETCHAR_PROTOTYPE int getchar (void)
#endif /* _RAISONANCE_ */

void console_init(void)
{
  //
    //  Clear the Idle Line Detected bit in the status register by a read
    //  to the UART1_SR register followed by a Read to the UART1_DR register.
    //
    unsigned char tmp = UART1_SR;
    tmp = UART1_DR;
    //
    //  Reset the UART registers to the reset values.
    //
    UART1_CR1 = 0;
    UART1_CR2 = 0;
    UART1_CR4 = 0;
    UART1_CR3 = 0;
    UART1_CR5 = 0;
    UART1_GTR = 0;
    UART1_PSCR = 0;
    //
    //  Now setup the port to 115200,n,8,1.
    //
    UART1_CR1_M = 0;        //  8 Data bits.
    UART1_CR1_PCEN = 0;     //  Disable parity.
    UART1_CR3_STOP = 0;     //  1 stop bit.
    UART1_BRR2 = 0x0a;      //  Set the baud rate registers to 115200 baud
    UART1_BRR1 = 0x08;      //  based upon a 16 MHz system clock.
    //
    //  Disable the transmitter and receiver.
    //
    UART1_CR2_TEN = 0;      //  Disable transmit.
    UART1_CR2_REN = 0;      //  Disable receive.
    //
    //  Set the clock polarity, lock phase and last bit clock pulse.
    //
    UART1_CR3_CPOL = 1;
    UART1_CR3_CPHA = 1;
    UART1_CR3_LBCL = 1;
    //
    //  Turn on the UART transmit, receive and the UART clock.
    //
    UART1_CR2_TEN = 1;
    UART1_CR2_REN = 1;
    UART1_CR3_CKEN = 1;
}

void uart_printf(char *message)
{
    char *ch = message;
    while (*ch)
    {
        uart_put_char(*ch);     //  Put the next character into the data transmission function.
        while (UART1_SR_TXE == 0);          //  Wait for transmission to complete.
        ch++;                               //  Grab the next character.
    }
}
void uart_print_float(double f)
{
  int i = (int) (f*10);
  if (i < 0)
    uart_put_char('-');
  else
   uart_put_char(' ');
  i = abs(i);
  i = i%1000;
  uart_put_char(i/100+'0');
  i = i%100;
  uart_put_char(i/10+'0');
  uart_put_char('.');
  i = i %10;
  uart_put_char(i+'0');
}
void uart_print_int(int i)
{
  if (i < 0)
    uart_put_char('-');
  else
   uart_put_char(' ');
  i = abs(i);
  i = i%1000000;
  uart_put_char(i/100000+'0');
  i = i%100000;
  uart_put_char(i/10000+'0');
  i = i%10000;
  uart_put_char(i/1000+'0');
  i = i%1000;
  uart_put_char(i/100+'0');
  i = i%100;
  uart_put_char(i/10+'0');
  i = i %10;
  uart_put_char(i+'0');
}
void uart_put_char(unsigned char c)
{
  UART1_DR = (unsigned char) c;     //  Put character into the data transmission register.
  while (UART1_SR_TXE == 0);          //  Wait for transmission to complete.
  if (c == '\n')
    UART1_DR = (unsigned char) '\r';     //  Put character into the data transmission register.
  while (UART1_SR_TXE == 0);          //  Wait for transmission to complete.
}

/**
  * @brief Retargets the C library printf function to the UART.
  * @param c Character to send
  * @retval char Character sent
  */
PUTCHAR_PROTOTYPE
{
  /* Write a character to the UART1 */
  uart_put_char(c);

  return (c);
}

/**
  * @brief Retargets the C library scanf function to the USART.
  * @param None
  * @retval char Character to Read
  */
GETCHAR_PROTOTYPE
{
#ifdef _COSMIC_
  char c = 0;
#else
  int c = 0;
#endif
  /* Loop until the Read data register flag is SET */
  //while (UART1_GetFlagStatus(UART1_FLAG_RXNE) == RESET);
    //c = UART1_ReceiveData8();
  return (c);
}

/*****END OF FILE****/
