/* stm8_pwm.c */

#include "stm8_pwm.h"

void stm8_pwm_init()
{
  /* Disable the Channel 1: Reset the CCE Bit, Set the Output State , 
  the Output N State, the Output Polarity & the Output N Polarity*/
  TIM1_CCER1 = 0;
  TIM1_CCER2 = 0;
  /* Set the Output State & Set the Output N State & Set the Output Polarity &
  Set the Output N Polarity */
  TIM1_CCER1 |= (uint8_t)0x77;
  TIM1_CCER2 |= (uint8_t)0x77;
  /* Reset the Output Compare Bits & Set the Output Compare Mode */
  TIM1_CCMR1 |= 0x70;//((uint8_t)TIM1_OCMODE_PWM2);
  TIM1_CCMR2 |= 0x70;//((uint8_t)TIM1_OCMODE_PWM2);
  TIM1_CCMR3 |= 0x70;//((uint8_t)TIM1_OCMODE_PWM2);
  TIM1_CCMR4 |= 0x70;//((uint8_t)TIM1_OCMODE_PWM2);
  /* Reset the Output Idle state & the Output N Idle state bits */
  TIM1_OISR &= 0;
  /* Set the Output Idle state & the Output N Idle state configuration */
  TIM1_OISR |= (uint8_t)0x55;  
  /* Set the Pulse value */
  TIM1_CCR1H = 0;
  TIM1_CCR1L = 0;
  TIM1_CCR2H = 0;
  TIM1_CCR2L = 0;  
  
  /* TIM1 Main Output Enable */
  TIM1_BKR |= 0x80;
}

void stm8_pwm_set_value(uint8_t pwm, uint16_t value)
{
    if ((pwm & STM8_PWM1)==STM8_PWM1)
    {
        /* Set the Capture Compare1 Register value */
        TIM1_CCR1H = (uint8_t)(value >> 8);
        TIM1_CCR1L = (uint8_t)(value);
    }
    if ((pwm & STM8_PWM2)==STM8_PWM2)
    {
        /* Set the Capture Compare1 Register value */
        TIM1_CCR2H = (uint8_t)(value >> 8);
        TIM1_CCR2L = (uint8_t)(value);
    }
    if ((pwm & STM8_PWM3)==STM8_PWM3)
    {
        /* Set the Capture Compare1 Register value */
        TIM1_CCR3H = (uint8_t)(value >> 8);
        TIM1_CCR3L = (uint8_t)(value);
    }
    if ((pwm & STM8_PWM4)==STM8_PWM4)
    {
        /* Set the Capture Compare1 Register value */
        TIM1_CCR4H = (uint8_t)(value >> 8);
        TIM1_CCR4L = (uint8_t)(value);
    }
}
