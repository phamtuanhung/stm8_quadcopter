/* stm8_pwm.h */

#include <stdint.h>
#include <iostm8s103f3.h>

#define STM8_PWM1 0x01
#define STM8_PWM2 0x02
#define STM8_PWM3 0x04
#define STM8_PWM4 0x08

extern uint16_t pwm;

void stm8_pwm_init(void);
void stm8_pwm_set_value(uint8_t pwm, uint16_t value);

