/* stm8_i2c.c */

#include "stm8_i2c.h"

static uint16_t gTimeout = 0;

void stm8_i2c_init (void) {
    I2C_CR1 = 0;     //    Disable I2C before configuration starts. PE bit is bit 0
    I2C_FREQR = 16;                                         //    Set the internal clock frequency (MHz).
    I2C_CCRH &= ~I2C_FS;                     //    I2C running is standard mode.
    I2C_CCRL = 0x10;                                        //    SCL clock speed is 500 kHz.
    I2C_CCRH &= 0x00;   // Clears lower 4 bits "CCR"
    I2C_OARH &= ~I2C_ADDMODE;            //    7 bit address mode.
    I2C_OARH |= I2C_ADDCONF;                //    Docs say this must always be 1.
    I2C_TRISER = 17;
    I2C_CR1 = I2C_PE;   // Enables port
}

int8_t stm8_i2c_read (unsigned char *x) {
    gTimeout = 0;
    while((I2C_SR1 & I2C_RXNE)==0)
    {
        gTimeout++;
        if (gTimeout == STM8_I2C_TIMEOUT)
            return STM8_I2C_ERROR;
    }
    *x = I2C_DR;
    return 0;
}
void stm8_i2c_set_nak (void) {
    I2C_CR2 &= ~I2C_ACK;
}
void stm8_i2c_set_stop (void) {
    I2C_CR2 |= I2C_STOP;
}
int8_t stm8_i2c_send_reg (uint8_t rg) {
    volatile int reg;
    reg = I2C_SR1;
    reg = I2C_SR3;
    I2C_DR = rg;
    gTimeout = 0;
    while((I2C_SR1 & I2C_TXE)==0)
    {
        gTimeout++;
        if (gTimeout == STM8_I2C_TIMEOUT)
            return STM8_I2C_ERROR;
    }
    return 0;
}

int8_t stm8_i2c_send_address (uint8_t mode, uint8_t address) {
    volatile int reg;
    reg = I2C_SR1;
    I2C_DR = (address << 1) | mode;
    if (mode == STM8_I2C_READ) {
        I2C_OARL = 0;
        I2C_OARH = 0;
    }
    
    gTimeout = 0;
    while((I2C_SR1 & I2C_ADDR)==0)
    {
        gTimeout++;
        if (gTimeout == STM8_I2C_TIMEOUT)
            return STM8_I2C_ERROR;
    }
    if (mode == STM8_I2C_READ)
                I2C_SR1 &= ~I2C_ADDR;
    return 0;
}

int8_t stm8_i2c_set_start_ack (void) {
    I2C_CR2 = I2C_ACK | I2C_START;
    gTimeout = 0;
    while((I2C_SR1 & I2C_SB)==0)
    {
        gTimeout++;
        if (gTimeout == STM8_I2C_TIMEOUT)
            return STM8_I2C_ERROR;
    }
    return 0;
}

unsigned char stm8_i2c_read_register (uint8_t rg, uint8_t address) {
    volatile uint8_t reg;
    uint8_t x;
    int8_t error = 0;
    error |= stm8_i2c_set_start_ack ();
    error |= stm8_i2c_send_address (STM8_I2C_WRITE, address);
    error |= stm8_i2c_send_reg (rg);
    error |= stm8_i2c_set_start_ack ();
    error |= stm8_i2c_send_address (STM8_I2C_READ, address);
    
    // if (error!=0)
        // return STM8_I2C_ERROR;
    
    reg = I2C_SR1;
    reg = I2C_SR3;
    stm8_i2c_set_nak ();
    stm8_i2c_set_stop ();
    stm8_i2c_read (&x);
    return (x);
}
