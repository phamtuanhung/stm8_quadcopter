
#include "stm8_io.h"

exti_callback_function_t exti_callback_pointer;
/**
  * @brief  External Interrupt PORTA Interrupt routine
  * @param  None
  * @retval None
  */
//INTERRUPT_HANDLER(EXTI_PORTA_IRQHandler, 3)
//{
  /* In order to detect unexpected events during development,
     it is recommended to set a breakpoint on the following instruction.
  */
//  (*exti_callback_pointer)();
//  toggle_led();
//}
#pragma vector = 5
__interrupt void EXTI_PORTA_IRQHandler(void)
{
  (*exti_callback_pointer)();
  toggle_led();
}
void io_set_exti_callback(exti_callback_function_t function)
{
  exti_callback_pointer = function;
}
void io_exti_dump_fuction()
{
  
}
void io_init(void)
{
  //GPIO_Init(LED_GPIO_PORT, (GPIO_Pin_TypeDef)LED_GPIO_PINS, GPIO_MODE_OUT_PP_HIGH_FAST);
  PD_ODR = 0; //Turn off all pins
  PD_DDR_DDR2 = 1; //PortC, Bit 5 is output (PC5 - Data Direction Register)
  PD_CR1_C12 = 1; //PortC, Control Register 1, Bit 5 (PC5) set to Push-Pull
  PD_CR2_C22 = 1; //PortC, Control Register 2, Bit 5 (PC5) set to Push-Pull
  
  exti_callback_pointer = io_exti_dump_fuction;
  //GPIO_Init(BUTTON_GPIO_PORT, (GPIO_Pin_TypeDef)(BUTTON_GPIO_PINS), GPIO_MODE_IN_PU_IT);
  
  PA_ODR = 0;             //  All pins are turned off.
  PA_DDR = 0xff;          //  All pins are outputs.
  PA_CR1 = 0xff;          //  Push-Pull outputs.
  PA_CR2 = 0xff;          //  Output speeds up to 10 MHz.
  //
  //  Now configure the input pin.
  //
  PA_DDR_DDR2 = 0;        //  PA2 is input.
  PA_CR1_C12 = 0;         //  PA2 is floating input.
  
  //io_exti_sensitivity(EXTI_PORT_GPIOA, EXTI_SENSITIVITY_RISE_FALL);
  //EXTI->CR1 &= (uint8_t)(~0x03);
  //EXTI->CR1 |= (uint8_t)(EXTI_SENSITIVITY_RISE_FALL);
  
  EXTI_CR1_PAIS = 3;      //  Interrupt on falling edge.
  //EXTI_CR2_TLIS = 0;      //  Falling edge only.
}

void set_led(uint8_t state)
{
  if (state == ON)
    PD_ODR &= (uint8_t)(~LED_GPIO_PINS);
  else
    PD_ODR |= (uint8_t)LED_GPIO_PINS;
}
void toggle_led(void)
{
  PD_ODR ^= (uint8_t)LED_GPIO_PINS;
}

uint8_t io_read_data()
{
  return (PA_IDR)&BUTTON_GPIO_PINS;
}