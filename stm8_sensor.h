//
// Global do-everything catch-all header for STM8S103F3P6
//
#include <stdint.h>
#include <iostm8s103f3.h>

// I2C_CR1
#define I2C_NOSTRETCH	(1 << 7)
#define I2C_ENGC	(1 << 6)
#define I2C_PE		(1 << 0)

// I2C_CR2
#define I2C_SWRST	(1 << 7)
#define I2C_POS		(1 << 3)
#define I2C_ACK		(1 << 2)
#define I2C_STOP	(1 << 1)
#define I2C_START	(1 << 0)

// I2C_OARH
#define I2C_ADDMODE	(1 << 7)
#define I2C_ADDCONF	(1 << 6)

// I2C_CCRH
#define I2C_FS		(1 << 7)
#define I2C_DUTY	(1 << 6)

// I2C_ITR
#define I2C_ITBUFEN	(1 << 2)
#define I2C_ITEVTEN	(1 << 1)
#define I2C_ITERREN	(1 << 0)

// I2C_SR1
#define I2C_TXE		(1 << 7)
#define I2C_RXNE	(1 << 6)
#define I2C_STOPF	(1 << 4)
#define I2C_ADD10	(1 << 3)
#define I2C_BTF		(1 << 2)
#define I2C_ADDR	(1 << 1)
#define I2C_SB		(1 << 0)

#define I2C_CR1			*(unsigned char*)0x005210 // I2C control register 1   
#define I2C_CR2			*(unsigned char*)0x005211 // I2C control register 2   
#define I2C_FREQR		*(unsigned char*)0x005212 // I2C frequency register    
#define I2C_OARL		*(unsigned char*)0x005213 // I2C Own address register low  
#define I2C_OARH		*(unsigned char*)0x005214 // I2C Own address register high  
#define I2C_DR			*(unsigned char*)0x005216 // I2C data register    
#define I2C_SR1			*(unsigned char*)0x005217 // I2C status register 1   
#define I2C_SR2			*(unsigned char*)0x005218 // I2C status register 2   
#define I2C_SR3			*(unsigned char*)0x005219 // I2C status register 3   
#define I2C_ITR			*(unsigned char*)0x00521A // I2C interrupt control register   
#define I2C_CCRL		*(unsigned char*)0x00521B // I2C Clock control register low  
#define I2C_CCRH		*(unsigned char*)0x00521C // I2C Clock control register high  
#define I2C_TRISER		*(unsigned char*)0x00521D // I2C TRISE register    
#define I2C_PECR		*(unsigned char*)0x00521E // I2C packet error checking register  

#define ACCEL_XOUT_H     0x3B
#define ACCEL_XOUT_L     0x3C
#define ACCEL_YOUT_H     0x3D
#define ACCEL_YOUT_L     0x3E
#define ACCEL_ZOUT_H     0x3F
#define ACCEL_ZOUT_L     0x40
#define TEMP_OUT_H       0x41
#define TEMP_OUT_L       0x42
#define GYRO_XOUT_H      0x43
#define GYRO_XOUT_L      0x44
#define GYRO_YOUT_H      0x45
#define GYRO_YOUT_L      0x46
#define GYRO_ZOUT_H      0x47
#define GYRO_ZOUT_L      0x48

typedef struct
{
  int16_t gyro_x;
  int16_t gyro_y;
  int16_t gyro_z;
  int16_t accel_x;
  int16_t accel_y;
  int16_t accel_z;
}sensor_data_t;

void i2c_sensor_init(void);
void i2c_init (void);
void i2c_mpu6050_init(void);
sensor_data_t i2c_get_mpu6050(void);
unsigned char i2c_read_register (uint8_t rg);
void i2c_wait(uint8_t wait);
void i2c_mpu6050_calibrate(void);
