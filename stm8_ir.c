
#include <stdio.h>
#include "stm8_ir.h"
#include "stm8_timer.h"
#include "stm8_uart.h"
#include "stm8_pid.h"

int ir_data = 0;
uint8_t ir_count = 0;
uint8_t ir_receiving = 0;

extern uint16_t time_out;

void ir_exti_calback(void)
{
  time_out = 0;
  uint16_t data = timer_get_loop_time();
  if ((ir_receiving == 0)&&(data>270)&&(data<330))
  {
    ir_receiving = 1;
    timer_interval_disable();
  }
  if (ir_receiving == 1)
  {
    if (data > 350)
    {
      //printf("ir_data: %d\n",ir_data);
      ir_receiver(ir_data);
      ir_count = 0;
      ir_receiving = 0;
      ir_data=0;
      timer_interval_enable();
    }
    else
    {
      if ((ir_count>0)&&(ir_count%2)==0)
        if (data > 100)
          {
            ir_data |= (1<<(ir_count/2));
          }
      ir_count++;
    }
  }
  //uart_printf("data: ");
  //uart_print_int(data);
  //uart_printf("\n");
}
void ir_receiver(int data)
{
  switch (data)
  {
  case 292: /*v+*/
    if (pwm < 600) pwm += 10; break;
  case 294: /*v-*/
    if (pwm >10) pwm -= 10; break;
  case 374: /*o*/
    pwm = 500; break;
  case 296: /*x*/
    pwm = 0; pid_calibrate(); break;
  case 288: /*p+*/
    pid_adjust_k(1); break;
  case 290: /*p-*/
    pid_adjust_k(-1); break;
    
  case -26826: k_adjust = ADJ_P; break;
  case -26828: k_adjust = ADJ_I; break;
  case -26824: k_adjust = ADJ_D; break;
  case -26760: adjust = 10; break;
  case -26830: adjust = 1;  break;
  case -26832: adjust = 0.1; break;
  case -26758: adjust = 0.01; break;
  
  case 256: pwm = 100; break;
  case 258: pwm = 200; break;
  case 260: pwm = 300; break;
  case 262: pwm = 400; break;
  case 264: pwm = 500; break;
  case 266: pwm = 600; break;
  case 268: pwm = 700; break;
  case 270: pwm = 800; break;
  case 272: pwm = 900; break;
  
  }
  uart_printf("pwm: ");
  uart_print_int(pwm);
  uart_printf("   ");
  pid_print_k();
}