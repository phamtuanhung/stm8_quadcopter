
#include <stdio.h>
#include <iostm8s103f3.h>

void console_init(void);
void uart_printf(char *message);
void uart_put_char(unsigned char c);
void uart_print_float(double f);
void uart_print_int(int i);