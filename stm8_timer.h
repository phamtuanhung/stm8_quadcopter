
#include <stdint.h>
#include <iostm8s103f3.h>

typedef void (*interval_callback_function_t)(void);
typedef struct
{
  uint16_t s;
  uint16_t ms;
  uint16_t us;
} stm8time_t;

extern uint16_t pwm;

void Delay (uint16_t nCount);
void InitialiseSystemClock(void);
void timer_init(void);
void timer_timer1_init(void);
void timer_timer2_init(void);
void timer_set_interval(interval_callback_function_t function, uint16_t ms);
void timer_interval_enable();
void timer_interval_disable();
void timer_dump_interval(void);
stm8time_t timer_get_time_us(void);
uint16_t timer_get_loop_time(void);
void timer_set_last_time(void);
uint16_t timer_get_counter(void);
void timer_pwm_init(void);
void timer_pwm1_init(void);
void timer_pwm2_init(void);
void timer_pwm3_init(void);
void timer_pwm4_init(void);
void timer_set_pwm1(uint16_t value);
void timer_set_pwm2(uint16_t value);
void timer_set_pwm3(uint16_t value);
void timer_set_pwm4(uint16_t value);

