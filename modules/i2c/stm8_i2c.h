/* stm8_i2c.h */

#include <stdint.h>
#include <iostm8s103f3.h>

// I2C_CR1
#define I2C_NOSTRETCH	(1 << 7)
#define I2C_ENGC	(1 << 6)
#define I2C_PE		(1 << 0)

// I2C_CR2
#define I2C_SWRST	(1 << 7)
#define I2C_POS		(1 << 3)
#define I2C_ACK		(1 << 2)
#define I2C_STOP	(1 << 1)
#define I2C_START	(1 << 0)

// I2C_OARH
#define I2C_ADDMODE	(1 << 7)
#define I2C_ADDCONF	(1 << 6)

// I2C_CCRH
#define I2C_FS		(1 << 7)
#define I2C_DUTY	(1 << 6)

// I2C_ITR
#define I2C_ITBUFEN	(1 << 2)
#define I2C_ITEVTEN	(1 << 1)
#define I2C_ITERREN	(1 << 0)

// I2C_SR1
#define I2C_TXE		(1 << 7)
#define I2C_RXNE	(1 << 6)
#define I2C_STOPF	(1 << 4)
#define I2C_ADD10	(1 << 3)
#define I2C_BTF		(1 << 2)
#define I2C_ADDR	(1 << 1)
#define I2C_SB		(1 << 0)

#define I2C_CR1			*(unsigned char*)0x005210 // I2C control register 1   
#define I2C_CR2			*(unsigned char*)0x005211 // I2C control register 2   
#define I2C_FREQR		*(unsigned char*)0x005212 // I2C frequency register    
#define I2C_OARL		*(unsigned char*)0x005213 // I2C Own address register low  
#define I2C_OARH		*(unsigned char*)0x005214 // I2C Own address register high  
#define I2C_DR			*(unsigned char*)0x005216 // I2C data register    
#define I2C_SR1			*(unsigned char*)0x005217 // I2C status register 1   
#define I2C_SR2			*(unsigned char*)0x005218 // I2C status register 2   
#define I2C_SR3			*(unsigned char*)0x005219 // I2C status register 3   
#define I2C_ITR			*(unsigned char*)0x00521A // I2C interrupt control register   
#define I2C_CCRL		*(unsigned char*)0x00521B // I2C Clock control register low  
#define I2C_CCRH		*(unsigned char*)0x00521C // I2C Clock control register high  
#define I2C_TRISER		*(unsigned char*)0x00521D // I2C TRISE register    
#define I2C_PECR		*(unsigned char*)0x00521E // I2C packet error checking register

#define STM8_I2C_READ              1
#define STM8_I2C_WRITE             0
#define STM8_I2C_TIMEOUT        1600
#define STM8_I2C_ERROR          (-1)

static void delay (int time_ms);
void stm8_i2c_init (void);
int8_t stm8_i2c_read (unsigned char *x);
void stm8_i2c_set_nak (void);
void stm8_i2c_set_stop (void);
int8_t stm8_i2c_send_reg (uint8_t rg);
int8_t stm8_i2c_send_address (uint8_t mode, uint8_t address);
int8_t stm8_i2c_set_start_ack (void);
unsigned char stm8_i2c_read_register (uint8_t rg, uint8_t address);
