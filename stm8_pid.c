
#include <stdlib.h>
#include <math.h>
#include "stm8_pid.h"
#include "stm8_sensor.h"
#include "stm8_timer.h"
#include "stm8_uart.h"

double last_angle_x = 0;
double last_angle_y = 0;
double errSum_x = 0;
double errSum_y = 0;
double setpoint_x = 0;
double setpoint_y = 4.5;
double kp = 5;
double ki = 0;
double kd = 0.5;
int pwm_offset_x = 0;//-25;
int pwm_offset_y = 0;//20;

uint8_t k_adjust = ADJ_P;
double adjust = 1;

#define RAD_TO_DEG 57.29577951308232087679815481

void pid_update_pwm(void)
{
  sensor_data_t sensor_data;
  sensor_data = i2c_get_mpu6050();//i2c_read_register (0x68, 0x3B);
  double dt = 0.02;
  
  double rollx = atan2( (int)sensor_data.accel_z, (int)sensor_data.accel_x) * RAD_TO_DEG - 90;
  double rolly = atan2( (int)sensor_data.accel_z, (int)-sensor_data.accel_y) * RAD_TO_DEG - 90;
  
  double time_constant = 0.5;
  double A = time_constant/(time_constant+dt);
//  double angle_x = A * (last_angle_x + ((double) (sensor_data.gyro_z*dt)/(131))) + (1-A) * ((rollx));
//  double angle_y = A * (last_angle_y + ((double) (sensor_data.gyro_y*dt)/(131))) + (1-A) * ((rolly));
  
  double angle_x = rollx;
  double angle_y = rolly;
  
  double error_x = setpoint_x - angle_x;
  errSum_x+= error_x;
  double de_x = (setpoint_x - angle_x) - (setpoint_x - last_angle_x); //de = (last_input - input);  
  int pwm_x = (int)(kp * error_x + ki*errSum_x*dt + kd*de_x/dt);/* P + I + D */
  
  double error_y = setpoint_y - angle_y;
  errSum_y+= error_y;  
  double de_y = (setpoint_y - angle_y) - (setpoint_y - last_angle_y); //de = (last_input - input);  
  int pwm_y = (int)(kp * error_y + ki*errSum_y*dt + kd*de_y/dt);/* P + I + D */
  
  last_angle_x = angle_x;
  last_angle_y = angle_y;
  
  //uart_printf("pwm_x: ");
  //uart_print_int(pwm_x);
  //uart_printf("    pwm_y: ");
  //uart_print_int(pwm_y);
  //uart_printf("\n");
  
 if ((abs((int)error_x) > 20)||(abs((int)error_y) > 20))
  {
    pwm_x = 0;
    pwm_y = 0;
  }
  pwm_x += pwm_offset_x;
  pwm_y += pwm_offset_y;
 
  if (pwm < 50)
  {
    pwm_x = 0;
    pwm_y = 0;
  }
  pwm_x = 0;
  pwm_y = 0;
  timer_set_pwm1(pwm - pwm_x);
  timer_set_pwm2(pwm + pwm_x);
  timer_set_pwm3(pwm + pwm_y);
  timer_set_pwm4(pwm - pwm_y);
    
  //uart_printf("pwm_x: ");
  //uart_print_int(pwm_x);
  //uart_printf("    pwm_y: ");
  //uart_print_int(pwm_y);
  //uart_printf("\n");
  
  uart_printf("   angle_x: ");
  uart_print_float(angle_x);
  uart_printf("    angle_y: ");
  uart_print_float(angle_y);
//  uart_printf("\n");
  
  
  uart_printf("    Accel: ");
  uart_print_int(sensor_data.accel_x);
  uart_printf(", ");
  uart_print_int(sensor_data.accel_y);
  uart_printf(", ");
  uart_print_int(sensor_data.accel_z);
  
  uart_printf("   Gyro: ");
  uart_print_int(sensor_data.gyro_x);
  uart_printf(", ");         
  uart_print_int(sensor_data.gyro_y);
  uart_printf(", ");         
  uart_print_int(sensor_data.gyro_z);
  
  uart_printf("\n");
  //printf("Accel: %5d, %5d, %5d   ",sensor_data.accel_x,sensor_data.accel_y,sensor_data.accel_z);
  //printf("Gyro: %5d, %5d, %5d",sensor_data.gyro_x,sensor_data.gyro_y,sensor_data.gyro_z);
  //printf("\n");  
}

void pid_calibrate(void)
{
  //setpoint_x = last_angle_x;
  //setpoint_y = last_angle_y;
}

void pid_adjust_k(int8_t c)
{
  if (k_adjust == ADJ_P)
  {
    kp += c*adjust;
    if (kp<=0)
      kp = 1;
  }
  else if (k_adjust == ADJ_I)
  {
    ki += c*adjust;
    if (ki<0)
      ki = 0;
  }
  else
  {
    kd += c*adjust;
    if (kd<0)
      kd = 0;
  }
}
void pid_update_k(void)
{
  
}
void pid_print_k(void)
{
  uart_printf("kp: ");
  uart_print_float(kp);
  uart_printf("   ki: ");
  uart_print_float(ki);
  uart_printf("   kd: ");
  uart_print_float(kd);
  
  uart_printf("    angle_x: ");
  uart_print_float(setpoint_x - last_angle_x);
  uart_printf("    angle_y: ");
  uart_print_float(setpoint_y - last_angle_y);
  
  uart_printf("\n");
}
