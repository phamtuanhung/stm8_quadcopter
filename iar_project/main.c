/* main.c */
#include <intrinsics.h>
#include <intrinsics.h>

#include "stm8_gpio.h"
#include "stm8_uart.h"
#include "stm8_timer.h"
#include "stm8_pwm.h"
#include "stm8_sensor.h"

void interval_function(void);

void main( void )
{
    InitialiseSystemClock();
    stm8_gpio_init();
    stm8_gpio_set_led(ON);
    stm8_uart_init();
    stm8_uart_printf("\n\n*********************************\n");
    stm8_uart_printf("*        STM8_QUADCOPTER        *\n");
    stm8_uart_printf("*********************************\n");
    stm8_gpio_set_led(OFF);
    stm8_timer_init();
    stm8_timer_set_interval(interval_function, 500 /*ms*/);
    stm8_pwm_init();
    __enable_interrupt();
    stm8_sensor_init();
    uint16_t counter;
    counter = 0;
    while(1)
    {
        stm8_timer_delay(500);
        //stm8_uart_print_int(stm8_timer_couter_get_loop());
        //stm8_uart_printf("\n");
        stm8_pwm_set_value(STM8_PWM1|STM8_PWM2|STM8_PWM3|STM8_PWM4,counter*50);
        counter = (counter + 1)%10;
        
        sensor_data_t sensor_data;
        sensor_data = stm8_sensor_get_value();
        stm8_uart_printf("    Accel: ");
        stm8_uart_print_int(sensor_data.accel_x);
        stm8_uart_printf(", ");
        stm8_uart_print_int(sensor_data.accel_y);
        stm8_uart_printf(", ");
        stm8_uart_print_int(sensor_data.accel_z);
        
        stm8_uart_printf("   Gyro: ");
        stm8_uart_print_int(sensor_data.gyro_x);
        stm8_uart_printf(", ");         
        stm8_uart_print_int(sensor_data.gyro_y);
        stm8_uart_printf(", ");         
        stm8_uart_print_int(sensor_data.gyro_z);
        
        stm8_uart_printf("\n");
    }
}

void interval_function(void)
{
    stm8_gpio_toggle_led();
}
